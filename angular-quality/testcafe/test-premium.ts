import { Selector } from 'testcafe';

fixture(`Premium Fixture`)
  .page(`http://localhost:4200`);


test('Test Premium Feature', async t => {
  await t
    .typeText('#customerName', 'H.Hirsch')
    .typeText('#customerId', '30000')
    .takeScreenshot()
    .click('section form .btn.btn-primary')
    .expect(Selector('.row h2.col-12').innerText).contains('Premiumangebote');
});
